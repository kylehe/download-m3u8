const {spawn} = require('child_process');
const fse = require('fs-extra');
const url = require('url');
const path = require('path');
const os = require('os');
const log4js = require('log4js');
const uuidv4 = require('uuid/v4');
const Router = require('koa-router');
let router = new Router({
    prefix: '/m3u8'
});
module.exports = router;

let logger = log4js.getLogger();
logger.level = process.env.LOG_LEVEL || 'info';

const allowProtocols = [
    'http:', 'https:'
];
const allowedHostnames = [
    'media-qncdn.ruguoapp.com'
];
const tmpDownloadDir = path.join(os.tmpdir(), 'm3u8-download')
logger.info(`TMP DOWNLOAD DIR: ${tmpDownloadDir}`)
fse.ensureDirSync(tmpDownloadDir)

router.get('/download/:filename', async (ctx, next) => {
    let {filename} = ctx.params
    if (/\.{2,}\//.test(filename)) {
        ctx.status = 403
        return
    }
    let filepath = path.join(tmpDownloadDir, filename)
    if (!(await fse.exists(filepath))) {
        ctx.status = 404;
        ctx.set('X-status', 'downloading')
        return
    }
    ctx.set('X-status', 'ready')
    ctx.body = fse.createReadStream(filepath)
    setTimeout(function () {
        fse.remove(filepath)
    }, 3e3)
})

router.post('/download', (ctx, next) => {
    let {url: m3u8Url} = ctx.request.body;

    let {protocol, hostname, pathname} = url.parse(m3u8Url)
    if (!allowProtocols.includes(protocol) || !allowedHostnames.includes(hostname)) {
        ctx.status = 403
        return;
    }

    let filename = `${uuidv4()}.mp4`
    let savePath = path.join(tmpDownloadDir, `${uuidv4()}.mp4`);
    download(m3u8Url, savePath).then(function () {
        fse.rename(savePath, path.join(tmpDownloadDir, filename))
    }).catch(function () {
        fse.writeFile(path.join(tmpDownloadDir, filename), '')
    })

    ctx.body = {
        url: url.format({
            protocol: ctx.request.protocol,
            host: ctx.request.host,
            pathname: `/m3u8/download/${filename}`
        })
    }
});

function download(url, dest) {
    logger.info(`Download url: ${url} -> ${dest}`);
    return new Promise(function (resolve, reject) {
        let ffmpeg = spawn('ffmpeg', ['-i', url, dest])
        ffmpeg.stdout.on('data', function (data) {
            logger.debug(data)
        })
        ffmpeg.stderr.on('data', function (data) {
            logger.debug(data)
        })
        ffmpeg.on('close', function (code) {
            if (code) {
                logger.info(`Fail download url: ${url}`)
                reject()
                return
            }
            logger.info(`Success download url: ${url}`)
            resolve()
        });
    })
}
