const path = require('path');
const Koa = require('koa');
const koaBody = require('koa-body');

let app = new Koa();

app.use(
    koaBody({
        jsonLimit: '5mb'
    })
);

let router = require('./routers/m3u8');
app.use(router.routes()).use(router.allowedMethods())

let port = process.env.PORT || 3000;
let host = process.env.HOST || '127.0.0.1';
app.listen(port, host, function () {
    console.log(`Server is running on ${host}:${port}`);
});
